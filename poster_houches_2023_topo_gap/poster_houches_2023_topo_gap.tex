\documentclass[final, 24pt, a0paper, portrait, professionalfonts]{beamer}
\usepackage[orientation=portrait, size=a0, scale=1.25]{beamerposter}
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{lipsum}  % Pour générer du texte factice, vous pouvez le supprimer
\usepackage[capitalise]{cleveref}
\usepackage{bbm}

\renewcommand{\vec}[1]{{\mathbf #1}}

% Configuration du thème du poster
\usetheme{Copenhagen}
\usecolortheme{seahorse}

\usefonttheme{professionalfonts} % using non standard fonts for beamer
% Titre du poster
\title{Topological Photonic Band Gaps in Honeycomb Atomic Arrays}
\author{École de Physique des Houches}

\definecolor{myblue}{RGB}{0,0,200}
\definecolor{mywhite}{RGB}{255,255,255}

\newenvironment<>{varblock}[2][\textwidth]{% 
  \setlength{\textwidth}{#1} 
  \begin{actionenv}#3% 
  \def\insertblocktitle{\Large\begin{center}\color{mywhite}{#2}\end{center}\par}% 
  \par% 
  \usebeamertemplate{block begin}} 
{\par% 
  \usebeamertemplate{block end}% 
  \end{actionenv}}


\begin{document}

% Création du poster
\begin{frame}[t]
\begin{columns}[t]
\setbeamertemplate{caption}[numbered]
% Première colonne
\begin{column}{0.32\linewidth}
\vspace{10cm}
  \begin{varblock}{Introduction}
    {\bf
The spectrum of excitations of a two-dimensional, planar honeycomb lattice of two-level atoms coupled by the in-plane electromagnetic field may exhibit band gaps that can be opened either by applying an external magnetic field or by breaking the symmetry between the two triangular sublattices of which the honeycomb one is a superposition. We establish the conditions of band gap opening, compute the width of the gap, and characterize its topological property by a topological index (Chern number). The topological nature of the band gap leads to inversion of the population imbalance between the two triangular sublattices for modes with frequencies near band edges. Surrounding the lattice by a Fabry-P\'{e}rot cavity with small intermirror spacing $d < \pi/k_0$, where $k_0$ is the free-space wave number at the atomic resonance frequency, renders the system Hermitian by suppressing the leakage of energy out of the atomic plane without modifying its topological properties. }
\end{varblock}

  \begin{varblock}{Honeycomb atomic lattice in the free space}

\begin{minipage}{0.45\textwidth}
\begin{itemize}
\item  $\Delta_{\vec{B}} = g_e \mu_B |\vec{B}|/\hbar\Gamma_0$
\item $\Delta_{AB} = (\omega_B - \omega_A)/2\Gamma_0$
\end{itemize}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
    \begin{figure}[h]
  \centering
     \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
     \includegraphics[page=1,width=1\textwidth]{2level.pdf}
     \end{tabular}
    \end{figure}
\end{minipage}
    

\begin{align*}
{\hat{\mathbb{H}}} &=\sum_{n=1}^{N} \sum\limits_{m=-1}^1 \left[\hbar\omega_{A,B}+m g_e \mu_{B} |\vec{B}| -i \frac{\hbar\Gamma_{0}}{2}\right]  \left|e_{nm}\right\rangle\left\langle e_{nm}\right| \nonumber\\
&+\frac{3 \pi \hbar \Gamma_{0}}{k_{0}} \sum_{n \neq n'}^{N} \sum\limits_{m,m'=-1}^{1} \left[ {\hat d}_{eg} {\hat{\mathcal{G}}} \left(\mathbf{r}_{n}, \mathbf{r}_{n'}\right) {\hat d}_{eg}^{\dagger} \right]_{m m'} \left| e_{nm}\right\rangle\left\langle e_{n'm'} \right|
\end{align*}
    
\begin{figure}[h]
  \centering
   \begin{tabular}{@{}c@{\hspace{0.5cm}}c@{}}
     \includegraphics[page=1,width=.5\textwidth]{lattice.pdf}
     \includegraphics[page=1,width=.5\textwidth]{BZ.pdf} 
   \end{tabular}
    \caption{$(a)$ Honeycomb lattice of two-level atoms with interatomic spacing $a$. The lattice can be seen as a superposition of two triangular sublattices $A$ (blue disks) and $B$ (red disks) with atoms $A$ and $B$ having single-atom ground and excited states $|g_ {n} \rangle$ and  $|e_{nm} \rangle$ ($m = 0, \pm 1$) with energies $E_g$ and $E_e^{(A,B)} = E_g + \hbar \omega_{A,B}$, respectively. The wavy line indicates coupling of each atom to all the others via the electromagnetic field. The lattice is placed in a static magnetic field $\textbf{B}$ perpendicular to the atomic plane.
$\textbf{a}_1$,  $\textbf{a}_2$ and $\textbf{a}_3$ are basis vectors of the lattice.
$(b)$ The first Brillouin zone of the honeycomb atomic lattice. The purple path is followed to construct the band diagram in \cref{fig:bd}. The dashed circle delimits the free-space light cone $|\textbf{k}|<k_0$.}
 \label{fig:lattice}
\end{figure}
\begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
       \includegraphics[page=1,width=1\textwidth]{band_diagram_0.pdf} 
   \end{tabular}
    \caption{Band diagrams of a honeycomb lattice of atoms coupled via the electromagnetic field for $k_0a=2\pi\times 0.05$ [see \cref{fig:lattice}(a)] and four different pairs of $\Delta_{\vec{B}}$, $\Delta_{AB}$. Horizontal axis corresponds to the path shown in purple in the 2D Brillouin zone in \cref{fig:lattice}(b). Color code corresponds to the decay rate $\Gamma$ of quasimodes capped at $\Gamma/\Gamma_0 = 100$ (i.e., all $\Gamma/\Gamma_0 \geq 100$ are shown in yellow). We label bands from bottom to top by an index $\alpha = 1$--4. Gray shaded areas are band gaps between the second and third spectral bands, gray dashed lines delimit the free-space light cone \(|\textbf{k}| \leq k_0\) where $\Gamma > 0$.}
 \label{fig:bd}
\end{figure}
\end{varblock}



\end{column}

% Deuxième colonne
\begin{column}{.32\linewidth}
  \vspace{10cm}

  \begin{varblock}{Width of the band gap}
    \begin{eqnarray*}
\Delta_{\text{gap}} = 2 \times 
\begin{cases}
 || \Delta_{\textbf{B}} | - | \Delta_{AB}||,  &|\Delta_{\textbf{B}}| < \Delta_{\mathbf{B}}^{(1)} \\
| \Delta_{\textbf{B}}^{(1)} - | \Delta_{AB}||,  &\Delta_{\mathbf{B}}^{(1)} < |\Delta_{\textbf{B}}| <\Delta_{\mathbf{B}}^{(2)} \\
| \Delta_{\textbf{B}}^{(1)} - | \Delta_{AB}|| \\+ \Delta_{\textbf{B}}^{(2)} -  |\Delta_{\textbf{B}}|, &\Delta_{\mathbf{B}}^{(2)} < |\Delta_{\textbf{B}}| < \Delta_{\mathbf{B}}^{(3)} \\
0, &|\Delta_{\textbf{B}}| > \Delta_{\mathbf{B}}^{(3)} = \\ &| \Delta_{\textbf{B}}^{(1)} - | \Delta_{AB}|| + \Delta_{\textbf{B}}^{(2)}
\end{cases}
\label{eqn:wgap}
\end{eqnarray*}
\begin{figure}
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
     \includegraphics[page=1,width=0.9\textwidth]{gap.pdf}
   \end{tabular}
         \caption{(a)Chern number. (b) Width of the gap between the second and third bands in the band diagram of Fig.\ \ref{fig:bd} as a function of $\Delta_{\textbf{B}}$ for three different values of $\Delta_{AB}$ and fixed $k_0a = 2\pi \times 0.05$. Symbols show $\Delta_{\text{gap}}$ determined via numerical diagonalization of the Hamiltonian $\hat{{\cal H}}(\vec{k})$, gray dashed lines represent the analytical result (\ref{eqn:wgap}).}
 \label{fig:gap}
\end{figure}

\begin{figure}
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
     \includegraphics[page=1,width=0.9\textwidth]{gap_dB_cst.pdf}
   \end{tabular}
 \caption{$(a)$ Maximum width of the gap $\max(\Delta_{\text{gap}})$  between the second and third bands in the band diagram of Fig.\ \ref{fig:bd} as a function of $\Delta_{AB}$ for three different values of $k_0a$. Solid lines show numerical results obtained for $\Delta_{\textbf{B}} = 30$. Dashed lines correspond to the formula in the second line of Eq.\ (\ref{eqn:wgap}).
$(b)$ Log-log plot of $\max(\Delta_{\text{gap}})$ as a function of $k_0a$ at $\Delta_{AB}=0$. Symbols show numerical results obtained for $\Delta_{\textbf{B}}=12$. Gray dashed line is a power-law fit $\max(\Delta_{\text{gap}}) = A(k_0a/2\pi)^{-3}$ for $k_0a/2\pi < 0.05$ with the best-fit value $A = 3.24 \times 10^{-3}$.}
 \label{fig:saturation}\end{figure}
\end{varblock}

  

    \begin{varblock}{Honeycomb lattice in a Fabry-P\'{e}rot cavity}
          \begin{figure}[t]
   \centering
     \includegraphics[page=1,width=.95\textwidth]{plates.png}
    \end{figure}
    \begin{figure}[t]
   \centering
     \includegraphics[page=1,width=.45\textwidth]{re_G.pdf}
     \includegraphics[page=1,width=.45\textwidth]{im_G.pdf}
      \caption{Shift of the resonance frequency (a) and decay rate of the excited state (b) of a single atom in the middle of a Fabry-P\'{e}rot cavity for the dipole moment of the atomic transition parallel to the mirrors (red solid lines).}
    \end{figure}
\begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
       \includegraphics[page=1,width=1\textwidth]{band_diagram_2.pdf} 
   \end{tabular}
    \caption{Same as \cref{fig:bd} but for a honeycomb atomic lattice placed in the middle of a Fabry-P\'{e}rot cavity of width $k_0d=2$. The band diagrams of \cref{fig:bd} are also shown for comparison (gray points).}
 \label{fig:bd_2}
\end{figure}
  \end{varblock}




\end{column}


\begin{column}{.32\linewidth}
  \vspace{10cm}
  \begin{varblock}{Topological properties of the band structure}
 \begin{align*}
C_{\alpha}={\frac{1}{2\pi}}\int_{\text{BZ}}\Omega_{\alpha}(\mathbf{k})d^{2}\mathbf{k} \quad \textrm{(Chern number)}
\label{chern}
\end{align*}

\begin{align*}
\Omega_{\alpha}({\bf
k})=i\left[\left\langle\frac{\partial\boldsymbol{\psi}_{\alpha}({\vec{k}})}{\partial
k_{x}}\biggm\lvert\frac{\partial\boldsymbol{\psi}_{\alpha}({\vec{k}})}{\partial
k_{y}}\right\rangle-\left\langle\frac{\partial\boldsymbol{\psi}_{\alpha}({\vec{k}})}{\partial
k_{y}}\biggm\lvert\frac{\partial\boldsymbol{\psi}_{\alpha}({\vec {k}})}{\partial
k_{x}}\right\rangle\right]
\end{align*}

    \begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
       \includegraphics[page=1,width=0.45\textwidth]{chern_BZ.pdf}     \includegraphics[page=1,width=.45\textwidth]{gap_and_chern_annotated.pdf}
   \end{tabular}

   \begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}

   \end{tabular}
 \caption{
 Width of the gap between the second and third bands of the spectrum of a honeycomb atomic lattice in the middle of a Fabry-P\'{e}rot cavity ($k_0 d = 2$), as a function of lattice spacing $a$, for a system that is topologically trivial (upper line) or not (lower line). Color code shows Chern number of the gap: $C=0$ (blue) or $C = 1$ (red). Horizontal dashed line shows the gap width in the limit $a \to \infty$.}
\end{figure}
    \end{figure}

    \begin{itemize}
\item $ W_{\alpha}^A(\textbf{k}) = \left|\boldsymbol{\psi}_{\alpha}^{A+}(\textbf{k}) \right|^2 + \left|\boldsymbol{\psi}_{\alpha}^{A-}(\textbf{k}) \right|^2 $
\item $ W_{\alpha}^B(\textbf{k}) = \left|\boldsymbol{\psi}_{\alpha}^{B+}(\textbf{k}) \right|^2 + \left|\boldsymbol{\psi}_{\alpha}^{B-}(\textbf{k}) \right|^2 $
    \end{itemize}
    
\begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
     \includegraphics[page=1,width=.9\textwidth]{bd_sites_A.pdf}
   \end{tabular}
    \caption{Topological (a) and trivial (b) band structures color-coded as a function of the weight of the quasimodes on the atomic sublattice $A$, $W_{\alpha}^A(\vec{k})$. Horizontal axes correspond to the path shown in purple in the 2D Brillouin zone in \cref{fig:lattice}(b). 
$(c)$ and $(d)$ show the second band as a function of $\vec{k}$ in the 2D Brillouin zone for band diagrams in panels (a) and (b), respectively.}
\label{fig:bd_sites_A}
\end{figure}
  \end{varblock}
  
  \begin{varblock}{Appendix}
    \subsection{}
\begin{figure}[t]
   \centering
   \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
    \includegraphics[page=1,width=.45\textwidth]{smaller_imag_part.pdf} 
   \end{tabular}
 \caption{Maximum decay rate for the band diagram of \cref{fig:bd_2} as a function of cut-off length $h$ (red circles). Blue dashed line shows a parabolic fit to the numerical data.}
 \label{fig:gmax}
\end{figure}

  \vspace{-2cm}
\begin{eqnarray*}
\hat{G}_{\text{FP}}^{(\text{reg})}(\vec{r} = 0)  &=& 
\sum\limits_{n=-\infty}^{\infty} (-1)^n
 \int\frac{d^3 \vec{q}}{(2\pi)^3} \hat{G}(\vec{q}) e^{i \vec{q}_z n d} e^{-h^2 q^2/2}
\nonumber \\
&=& \hat{G}^{(\text{reg})}(\vec{r} = 0)
+ \mathbbm{1}_2 \times
3 \left[
\frac{1}{k_0 d} \ln\left(1 + e^{-i k_0 d} \right) \right.
\nonumber \\
&+& \left. \frac{i}{(k_0 d)^2} \text{Li}_2 \left(-e^{-i k_0 d} \right) +
\frac{1}{(k_0 d)^3} \text{Li}_3 \left(-e^{-i k_0 d} \right)
\right]
\label{g3dcutplates}
\end{eqnarray*}
\end{varblock}



\begin{varblock}{References}
  \nocite{*}
  \bibliographystyle{siam}
  \bibliography{poster_houches_2023_topo_gap}
\end{varblock}

\end{column}

\end{columns}
\end{frame}

\end{document}
