\documentclass[final, 24pt, a0paper, portrait, professionalfonts]{beamer}
\usepackage[orientation=portrait, size=a0, scale=1.25]{beamerposter}
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{lipsum}  % Pour générer du texte factice, vous pouvez le supprimer
\usepackage[capitalise]{cleveref}
\usepackage{caption}
\usepackage{bbm}
\captionsetup{labelfont={color=myred}} 
\renewcommand{\vec}[1]{{\mathbf #1}}

% Configuration du thème du poster
\usetheme{CambridgeUS}
\usecolortheme{beaver}
\usepackage{xcolor} % Inclure le package xcolor

% Définir une nouvelle couleur en rouge (par exemple)
\definecolor{myred}{RGB}{200,0,0}

% Appliquer la nouvelle couleur aux titres de blocs
%% \setbeamercolor{block title}{fg=myred}

%% \usepackage{etoolbox}
%% \AtBeginEnvironment{block}{\setbeamercolor{block title}{bg=white,fg=myred}\usebeamerfont{block title}}

\newenvironment<>{varblock}[2][\textwidth]{% 
  \setlength{\textwidth}{#1} 
  \begin{actionenv}#3% 
  \def\insertblocktitle{\Large\begin{center}\color{myred}{#2}\end{center}\par}% 
  \par% 
  \usebeamertemplate{block begin}} 
{\par% 
  \usebeamertemplate{block end}% 
  \end{actionenv}}

\setbeamertemplate{blocks}[rounded][shadow=true]
\usefonttheme{professionalfonts} % using non standard fonts for beamer
% Titre du poster
\title{Topological transitions and Anderson localization of light in disordered atomic arrays}
\author{École de Physique des Houches}

\begin{document}

% Création du poster
\begin{frame}[t]
\begin{columns}[t]
\setbeamertemplate{caption}[numbered]
% Première colonne
\begin{column}{0.32\linewidth}
\vspace{10cm}
  \begin{varblock}{\begin{center}Introduction \end{center}}
    {\bf
We explore the interplay of disorder and topological phenomena in honeycomb lattices of atoms coupled by the electromagnetic field. On the one hand, disorder can trigger transitions between distinct topological phases and drive the lattice into the topological Anderson insulator state. On the other hand, the nontrivial topology of the photonic band structure suppresses Anderson localization of modes that disorder introduces inside the band gap of the ideal lattice. Furthermore, we discover that disorder can both open a topological pseudogap in the spectrum of an otherwise topologically trivial system and introduce spatially localized modes inside it. 
}
\end{varblock}

  \begin{varblock}{Light in a honeycomb atomic lattice}

\begin{minipage}{0.45\textwidth}
\begin{itemize}
\item  $\Delta_{\vec{B}} = g_e \mu_B |\vec{B}|/\hbar\Gamma_0$
\item $\Delta_{AB} = (\omega_B - \omega_A)/2\Gamma_0$
\end{itemize}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
    \begin{figure}[h]
  \centering
     \begin{tabular}{@{}c@{\hspace{.5cm}}c@{}}
     \includegraphics[page=1,width=1\textwidth]{2level.pdf}
     \end{tabular}
    \end{figure}
\end{minipage}
    

\begin{align*}
{\hat{\mathbb{H}}} &=\sum_{n=1}^{N} \sum\limits_{m=-1}^1 \left[\hbar\omega_{A,B}+m g_e \mu_{B} |\vec{B}| -i \frac{\hbar\Gamma_{0}}{2}\right]  \left|e_{nm}\right\rangle\left\langle e_{nm}\right| \nonumber\\
&+\frac{3 \pi \hbar \Gamma_{0}}{k_{0}} \sum_{n \neq n'}^{N} \sum\limits_{m,m'=-1}^{1} \left[ {\hat d}_{eg} {\hat{\mathcal{G}}} \left(\mathbf{r}_{n}, \mathbf{r}_{n'}\right) {\hat d}_{eg}^{\dagger} \right]_{m m'} \left| e_{nm}\right\rangle\left\langle e_{n'm'} \right|
\end{align*}
    
\begin{figure}[h]
  \centering
   \begin{tabular}{@{}c@{\hspace{0.5cm}}c@{}}
     \includegraphics[page=1,width=0.55\textwidth]{lattice.pdf}
   \end{tabular}

    \caption{Honeycomb lattice of two-level atoms with interatomic spacing $a$. The lattice can be seen as a superposition of two triangular sublattices $A$ (blue disks) and $B$ (red disks) with atoms $A$ and $B$ having single-atom ground and excited states $|g_ {n} \rangle$ and  $|e_{nm} \rangle$ ($m = 0, \pm 1$) with energies $E_g$ and $E_e^{(A,B)} = E_g + \hbar \omega_{A,B}$, respectively. The wavy line indicates coupling of each atom to all the others via the electromagnetic field. The lattice is placed in a static magnetic field $\textbf{B}$ perpendicular to the atomic plane.
$\textbf{a}_1$,  $\textbf{a}_2$ and $\textbf{a}_3$ are basis vectors of the lattice.}
 \label{fig:lattice}
\end{figure}

The effective Hamiltonian of the lattice takes the form of a $2N
\times 2N$ (we consider only TE modes) non-Hermitian matrix $\hat G$
composed of $2 \times 2$ blocks ${\hat G}_{mn}$ describing the
interaction of atoms $m$ and $n$ where ${\hat{\cal G}}(\vec{r})$ is
the dyadic Green's function (see {\color{myred}{Appendix}}) of Maxwell equations:
\begin{eqnarray*}
{\hat G}_{mn} &=& \delta_{mn} \left[\left( i \pm 2 \Delta_{AB} \right) \mathbbm{1} + 2 {\hat \sigma}_z \Delta_{\vec{B}} \right] 
\nonumber \\
&-& \frac{6\pi}{k_0}(1 - \delta_{mn}) {\hat d}_{eg} \hat{\cal G}(\vec{r}_m - \vec{r}_n) {\hat d}_{eg}^{\dagger} \;\;\;\;
\label{greene}
\end{eqnarray*}

\begin{figure}[h]
  \centering
   \begin{tabular}{@{}c@{\hspace{0.5cm}}c@{}}
     \includegraphics[page=1,width=0.95\textwidth]{dos_ipr.pdf}
   \end{tabular}
    \caption{(a) Density of states (DOS) for light in a honeycomb
      atomic lattice of $N=612$ sites. Blue represents the DOS for a
      systme with a magnetic field $\Delta_{\mathbf{B}}=12$ and no
      difference between sites while red represents the case where
      $\Delta_{AB}=0$ and there is no magnetic field. Dashed rectangle
      show where the zoom of (b) is made. (c) and (d) are bigger
      version of the intensity maps shown in (b). As these states are
      in the gap, they are localized on the edge of the sample}
 \label{fig:lattice}
\end{figure}

\end{varblock}

\vspace{2cm}

\end{column}

% Deuxième colonne
\begin{column}{.32\linewidth}
  \vspace{10cm}

  \begin{varblock}{Disorder and Bott index}
From
\begin{eqnarray*}
{\hat G} | R_{\alpha} \rangle  = \Lambda_{\alpha} | R_{\alpha} \rangle
\label{eigenr}\quad \textrm{and}\quad
\langle L_{\alpha} | {\hat G} = \langle L_{\alpha} | {\tilde \Lambda}_{\alpha}^*
\label{eigenl}
\end{eqnarray*}
We define Bott index as
\begin{eqnarray*}
  C_B(\omega) &=& \frac{1}{2\pi} \mathrm{Im} \mathrm{Tr} \ln [{\hat V}_X(\omega) {\hat V}_Y(\omega) {\hat V}_X^{-1}(\omega) {\hat V}_Y^{-1}(\omega)]\\
  &\approx & \frac{1}{2\pi} \mathrm{Im} \mathrm{Tr} \ln [{\hat V}_X(\omega) {\hat V}_Y(\omega) {\hat V}_X^{\dagger}(\omega) {\hat V}_Y^{\dagger}(\omega)]
\label{bott}
\end{eqnarray*}
where
\begin{eqnarray*}
{\hat V}_{X,Y}(\omega)  &=& {\hat P}(\omega) {\hat U}_{X,Y} {\hat P}(\omega)
\label{vxy}
\\
{\hat U}_X &=& \exp(i 2\pi {\hat X}/L)
\label{ux}
\\
{\hat U}_Y &=& \exp(i 2\pi {\hat Y}/L)
\label{uy}
\end{eqnarray*}
and ${\hat X}$ and ${\hat Y}$ are diagonal $2N \times 2N$ matrices containing coordinates $x_m$ and $y_m$ of atoms,
\begin{eqnarray*}
{\hat P}(\omega) = \sum_{\omega_{\alpha} \leq \omega} | R_{\alpha} \rangle \langle L_{\alpha} |
\label{proj}
\end{eqnarray*}
is a projector operator on quasimodes corresponding to frequencies $\omega_{\alpha}$ below $\omega$.

\begin{figure}
\includegraphics[width=\textwidth]{chern_bott.pdf}
\caption{\label{chern_bott} (a) Chern number of bands below the gap as function of $\Delta_{\vec{B}}$ and $\Delta_{AB}$. (b) Bott index for a frequency $\omega=\omega_g$ in the center of the gap computed for 180 sites. In both dashed lines show $|\Delta_{\vec{B}}|=|\Delta_{AB}|$}
\end{figure}

    
\begin{figure}
\includegraphics[width=0.95\textwidth]{fig50.pdf}
%% \vspace{-8.5cm}
    \caption{\label{fig50}
Closing of a topological band gap by disorder. (a) Average Bott index $\langle C_B \rangle$ as a function of disorder strength $W$ for $\Delta_{\vec{B}} = 5$ and $\Delta_{AB} = 0$. The white contour reproduced for reference in all plots (a)--(c) shows the level $\langle C_B \rangle = -0.5$. (b) Average edge DOS. Dashed lines show edge modes predicted by the perturbation theory. Frequencies of the lowest- and highest-frequency edge modes traced up to higher values of $W$ approximate band edges. (c) Average bulk IPR. Deep violet color corresponds to either very low IPR or $\text{DOS} = 0$ (no modes). (d) Average quasimode decay rate in units of the natural decay rate $\Gamma_0$ of an isolated atom.
}
\end{figure}

\begin{figure}
\includegraphics[width=0.95\textwidth]{figipr.pdf}
\vspace{-8.7cm}
    \caption{\label{figipr}
Impact of topological properties on spatial localization of modes. Mean bulk IPR in the vicinity of topologically trivial gaps for (a) $\Delta_{\vec{B}} = 0$, $\Delta_{AB} = 5$ and  (b) $\Delta_{\vec{B}} = 5$, $\Delta_{AB} = 10$.
}
\end{figure}

  \end{varblock}

  \begin{varblock}{Appendix}
    \label{sec:Appendix}
%%     For a mode $\psi_{\alpha}$ we define the inverse participation ratio as
%%     \begin{eqnarray*}
%% \text{IPR}_{\alpha} &=& \frac{\sum_{m=1}^N \left( \sum_{\sigma = \pm 1} |R_{\alpha m \sigma}|^2 \right)^2}{\left( \sum_{m=1}^N \sum_{\sigma = \pm 1} |R_{\alpha m \sigma}|^2 \right)^2}
%% \label{ipr}
%%     \end{eqnarray*}
The dyadic Green's function of maxwell equations as 
    \begin{align*}
      \hat{{\mathcal G}}(\vec{r}) = -\frac{e^{i k_0 r}}{4 \pi r}
\left[ P(i k_0 r) \mathbbm{1}
  + Q(ik_0 r) \frac{\vec{r} \otimes \vec{r}}{r^2} \right]
\label{eq:green}
    \end{align*}
    where $k_0 = \omega_0/c = 2\pi/\lambda_0$, $\omega_0 = (\omega_A + \omega_B)/2$ and   $P(u) = 1 - 1/u + 1/u^2$, $Q(u) = -1 + 3/u - 3/u^2$.
    We have also used
    \begin{eqnarray}
{\hat d}_{eg} = \frac{1}{\sqrt{2}}
\begin{bmatrix}
1 & i \\
-1 & i \\
\end{bmatrix}\quad \textrm{and} \quad {\hat \sigma}_{z} =
\begin{bmatrix}
1 & 0 \\
0 & -1 \\
\end{bmatrix}
\label{dmatrix}
\end{eqnarray}
  \end{varblock}

\end{column}


\begin{column}{.32\linewidth}
  \vspace{10cm}
  \begin{varblock}{Topological Anderson Insulator}
\begin{figure}
\includegraphics[width=0.95\textwidth]{fig55.pdf}
%% \vspace{-8cm}
\caption{\label{fig55}
Opening of a topological pseudogap by disorder. The four plots show the same quantities as Fig.\ \ref{fig50} but for $\Delta_{\vec{B}} = \Delta_{AB} = 5$ and disorder on atoms $B$ only.
}
\end{figure}


\begin{figure}
\includegraphics[width=\textwidth]{fig_bott.pdf}
%% \vspace{-2cm}
\caption{\label{figbott}
Average Bott index $\langle C_B \rangle$ calculated for four different values of $\Delta_{\vec{B}}$ around $\Delta_{\vec{B}} = \Delta_{AB} = 5$ for $a = \lambda_0/20$ in a rectangular sample. The white contour shows the boundary $\langle C_B \rangle = -0.5$ between the topologically nontrivial region where $\langle C_B \rangle = -1$ (violet) and the trivial region where $\langle C_B \rangle = 0$ (red). Ensemble averaging is performed over 60 independent realizations of disorder for each graph.   
} 
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{fig_3d.pdf}
\vspace{-6cm}
\caption{\label{fig3d}
(a) Phase diagram of the 2D disordered honeycomb atomic lattice in the 3D parameter space ($W$, $\omega$, $\Delta_{\vec{B}}$)  for $a =\lambda_0/20$ and $\Delta_{AB} = 5$. The blue surface shows the boundary between the topologically nontrivial (inside, $\langle C_B \rangle = -1$) and trivial (outside, $\langle C_B \rangle = 0$) regions of the parameter space. The orange plane $\Delta_{\vec{B}} = \Delta_{AB}$ separates the TI phase for $\Delta_{\vec{B}} > \Delta_{AB}$ from TAI phase for $\Delta_{\vec{B}} < \Delta_{AB}$.
(b) 3D density plot of average bulk IPR with the orange plane separating TI and TAI as in (a). For clarity, the values less than 0.006 are not shown (transparent). }
\end{figure}


  \end{varblock}

\begin{varblock}{References}
  \nocite{*}
  \bibliographystyle{siam}
  \bibliography{refstopo}
\end{varblock}

\end{column}

\end{columns}
\end{frame}

\end{document}
